﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Common;

namespace Client.Repositories
{
    public interface IRepo<TEntity>
    {
        Task Delete(int id);
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> GetById(int id);
        Task Insert(TEntity entity);
        Task Update(int id, TEntity entity);
    }
}