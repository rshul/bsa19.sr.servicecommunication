﻿using Client.ApiTesters;
using Client.Repositories;
using MessageWorker;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using ProjectStructure.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Client
{

    class Program
    {
        static void Main(string[] args)
        {
            //var repo = new ProjectsClientRepo();
            //var pt = new ProjectsApiTester(repo);
            //pt.GenerateProjects(10);
            //pt.SendProjects();
            //IEnumerable<ProjectDTO> prs = pt.GetAllProjects().GetAwaiter().GetResult();
            //var c = JsonConvert.SerializeObject(prs, Formatting.Indented);
            //var project = pt.GetOneProject((prs as List<ProjectDTO>)[0].Id).GetAwaiter().GetResult();
            //project.Name = "Paratrooper";
            //pt.UpdateProject(project).GetAwaiter();
            //pt.DeleteProject(2).GetAwaiter();
            //IEnumerable<ProjectDTO> prsa = pt.GetAllProjects().GetAwaiter().GetResult();
            //var ca = JsonConvert.SerializeObject(prs, Formatting.Indented);
            //Console.WriteLine(c);

            var sh = new SignalHub("https://localhost:5001/messages");
            sh.SignalOn(m => Console.WriteLine(m));
            sh.HubStart();

            var repo = new ProjectsClientRepo();
            var pt = new ProjectsApiTester(repo);
            IEnumerable<ProjectDTO> prs = pt.GetAllProjects().GetAwaiter().GetResult();
            ConsoleKeyInfo pressedKey;
            System.Console.WriteLine("Press esc to exit");
            do
            {
                pressedKey = Console.ReadKey(true);
                prs = pt.GetAllProjects().GetAwaiter().GetResult();
            } while (pressedKey.Key != ConsoleKey.Escape);

            sh.HubStop();
            var FromFileLogs = HttpService.GetObjectsAsync<LogData>("Logs").Result;
            var JsonLogs = JsonConvert.SerializeObject(FromFileLogs, Formatting.Indented);
            Console.WriteLine(JsonLogs);

            Console.ReadLine();

        }
    }
}
