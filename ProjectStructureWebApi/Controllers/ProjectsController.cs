
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using ProjectStructure.BLL.Hubs;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common;


namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        
        private IService<ProjectDTO> _projectService;
        private QueueServiceBeta _qs;

        public ProjectsController(IService<ProjectDTO> projectService, QueueServiceBeta qs, IConfiguration config)
        {
            _projectService = projectService;
            _qs =qs;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> Get()
        {
            var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            
            return Ok(_projectService.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
             var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            ProjectDTO foundEntity;
            try
            {
                foundEntity = _projectService.Get(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public int Post([FromBody] ProjectDTO value)
        {
             var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");

            return _projectService.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] ProjectDTO value)
        {
             var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            {
                if (id == value.Id)
                {
                    _projectService.Update(id, value);
                }
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
             var messageSource = (
                classSource: System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,
                methodSource: System.Reflection.MethodBase.GetCurrentMethod().Name);

            _qs.PushMessage($"{messageSource.classSource}.{messageSource.methodSource} was triggered ");
            try
            {
                _projectService.Delete(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();

        }

    }
}