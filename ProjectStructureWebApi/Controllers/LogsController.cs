using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProjectStructure.BLL.Services;
using MessageWorker;
using System;
using Newtonsoft.Json;


namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogsController : ControllerBase
    {
        private IConfiguration _config;

        public LogsController(IConfiguration config)
        {
            _config = config;

        }
        [HttpGet]
        public ActionResult<IEnumerable<LogData>> Get()
        {
            IEnumerable<LogData> LogsCollection;
            try
            {
                string path = _config.GetSection("PathLogsFile").Value;
                var tx = System.IO.File.ReadAllText(path);
                string logsAsJson = tx.Replace(Environment.NewLine, ",").TrimEnd(',');

                LogsCollection = JsonConvert.DeserializeObject<List<LogData>>($"[{logsAsJson}]");
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
                return Ok(new List<LogData>());
            }

            return Ok(LogsCollection);
        }
    }
}