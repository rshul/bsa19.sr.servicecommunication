using System;
using ProjectStructure.Common.Interfaces;
namespace ProjectStructure.Common
{
    public class ProjectDTO: IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Deadline { get; set; }
        public int Author_Id { get; set; }
        public int Team_Id { get; set; }
    }
}