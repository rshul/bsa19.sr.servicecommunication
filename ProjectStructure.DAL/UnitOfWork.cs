﻿using ProjectStructure.Common;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Repositories;

namespace ProjectStructure.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ProjectsContext _context = new ProjectsContext();
        private IRepository<ProjectDTO> _projectRepository;

        private IRepository<UserDTO> _userRepository;

        private IRepository<TeamDTO> _teamRepository;

        private IRepository<TaskStateDTO> _taskStateRepository;

        private IRepository<ProjectTaskDTO> _projectTaskRepository;

        public IRepository<ProjectDTO> ProjectRepository
        {
            get
            {
                if (_projectRepository == null)
                {
                    _projectRepository = new GenericRepository<ProjectDTO>(_context.Projects);
                }
                return _projectRepository;
            }
        }

        public IRepository<UserDTO> UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new GenericRepository<UserDTO>(_context.Users);
                }
                return _userRepository;
            }
        }
  
        public IRepository<TeamDTO> TeamRepository
        {
            get
            {
                if (_teamRepository == null)
                {
                    _teamRepository = new GenericRepository<TeamDTO>(_context.Teams);
                }
                return _teamRepository;
            }
        }
        

        public IRepository<TaskStateDTO> TaskStateRepository
        {
            get
            {
                if (_taskStateRepository == null)
                {
                    _taskStateRepository = new GenericRepository<TaskStateDTO>(_context.TaskStates);
                }
                return _taskStateRepository;
            }
        }

      

        public IRepository<ProjectTaskDTO> ProjectTaskRepository
        {
            get
            {
                if (_projectTaskRepository == null)
                {
                    _projectTaskRepository = new GenericRepository<ProjectTaskDTO>(_context.ProjectTasks);
                }
                return _projectTaskRepository;
            }
        }
        
    }
}
