﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MessageWorker
{
    public class QueueServiceWorkerBeta : IDisposable, IQueueService
    {
        private IConnection _connectionProducer;
        private IModel _channelProducer;
        private IConnection _connectionConsumer;
        private IModel _channelConsumer;
        private EventingBasicConsumer _consumer;

        public QueueServiceWorkerBeta()
        {
            ConfigureConnection();
        }

        private void ConfigureConnection()
        {
            var factory = new ConnectionFactory()
            {
                HostName = "localhost"
            };

            _connectionProducer = factory.CreateConnection();
            _connectionConsumer = factory.CreateConnection();
            _channelProducer = _connectionProducer.CreateModel();
            _channelConsumer = _connectionConsumer.CreateModel();

            _channelProducer.ExchangeDeclare(exchange: "FromWorker", type: ExchangeType.Direct);

            _channelConsumer.ExchangeDeclare(exchange: "ToWorker", type: ExchangeType.Direct);
            _channelConsumer.QueueDeclare(
                queue: "worker1",
                exclusive: false,
                durable: true,
                autoDelete: false);

            _channelConsumer.QueueBind(
                exchange: "ToWorker",
                queue: "worker1",
                routingKey: "w1");

            _consumer = new EventingBasicConsumer(_channelConsumer);
            _consumer.Received += MessageReceived;
            _channelConsumer.BasicConsume(
                queue: "worker1",
                autoAck: false,
                consumer: _consumer);

        }

        public void PushMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            _channelProducer.BasicPublish(
                exchange: "FromWorker",
                routingKey: "s1",
                basicProperties: null,
                body: body);
        }

        private void MessageReceived(object sender, BasicDeliverEventArgs e)
        {
            var body = e.Body;
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"messageWorker received {message}");
            var logToSave = new LogData { Message = message, MessageDateTime = DateTime.Now };
            string textLogToSave = JsonConvert.SerializeObject(logToSave, Formatting.None);
            try
            {
                using (StreamWriter sw = File.AppendText("WorkerMessageLogs.txt"))
                {
                    sw.WriteLine(textLogToSave);
                }
                PushMessage($"Successfully received");
            }
            catch (Exception exeption)
            {
                Console.WriteLine(exeption.Message);
                PushMessage($"Failed during handling a message");
            }
            finally
            {
                _channelConsumer.BasicAck(deliveryTag: e.DeliveryTag, multiple: false);
            }

        }

        public void Dispose()
        {
            _connectionProducer?.Dispose();
            _channelProducer?.Dispose();
            _connectionConsumer?.Dispose();
            _channelConsumer?.Dispose();
        }
    }
}
